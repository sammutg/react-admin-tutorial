// in src/posts.js
import React from 'react';
import { Filter, List, Edit, Create, Datagrid, ReferenceField, TextField, EditButton, DisabledInput, LongTextInput, ReferenceInput, SelectInput, SimpleForm, TextInput } from 'react-admin';

// SimpleList & Responsive components use for Mobile
import { Responsive, SimpleList } from 'react-admin';

const PostFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Rechercher" source="q" alwaysOn />
        <ReferenceInput label="Auteur" source="userId" reference="users" allowEmpty>
            <SelectInput optionText="name" />
        </ReferenceInput>
    </Filter>
);

export const PostList = (props) => (

    <List title="Billets" {...props} filters={<PostFilter />}>
        <Responsive small = 
            {

                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.id }
                    tertiaryText={record => new Date(record.published_at).toLocaleDateString()}
                />

            } 
            medium = { 

                <Datagrid>
                    <TextField label="" source="id" />
                    <ReferenceField label="Auteur" source="userId" reference="users">
                        <TextField source="name" />
                    </ReferenceField>
                    <TextField label="Titre" source="title" />
                    <TextField source="body" />
                    <EditButton />
                </Datagrid>

            }
        />
    </List>

);

const PostTitle = ({ record }) => {
    return <span>Post {record ? `"${record.title}"` : ''}</span>;
};

export const PostEdit = (props) => (
    <Edit title={<PostTitle />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <ReferenceInput label="User" source="userId" reference="users">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="title" />
            <LongTextInput source="body" />
        </SimpleForm>
    </Edit>
);

export const PostCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <ReferenceInput label="User" source="userId" reference="users">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="title" />
            <LongTextInput source="body" />
        </SimpleForm>
    </Create>
);