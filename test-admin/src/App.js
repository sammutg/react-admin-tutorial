// in src/App.js
import React from 'react';
import { Admin, Resource } from 'react-admin/lib';
import jsonServerProvider from 'ra-data-json-server';
import { PostList, PostEdit, PostCreate } from './posts';
import { UserList } from './users';

import PostIcon from '@material-ui/icons/Book';
import UserIcon from '@material-ui/icons/Group';

import Dashboard from './Dashboard';
import authProvider from './authProvider';

const dataProvider = jsonServerProvider('http://jsonplaceholder.typicode.com');
// const App = () => <Admin dataProvider={dataProvider} />;
const App = () => (
    <Admin authProvider={authProvider} dashboard={Dashboard} dataProvider={dataProvider}>
        <Resource name="posts" list={PostList} edit={PostEdit} create={PostCreate} options={{ label: 'Billets' }} icon={PostIcon}/>
        <Resource name="users" list={UserList} options={{ label: 'Auteurs' }} icon={UserIcon} />
    </Admin>
);

export default App;