// in src/Dashboard.js
import React from 'react';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

export default () => (
    <Card>
        <CardHeader title="Dashboard" />
        <CardContent>
          <Typography color="textSecondary"> Billet du jour </Typography>
          <Typography variant="headline" component="h2">
           Titre du billet
          </Typography>
          <Typography color="textSecondary"> Nom de l'auteur </Typography>
          <Typography component="p">
            Long Text Input
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small">Editer</Button>
        </CardActions>
    </Card>
);