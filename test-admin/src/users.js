// in src/users.js
import React from 'react';
import { List, Datagrid, EmailField, TextField } from 'react-admin';

export const UserList = (props) => (
    <List title="Auteurs" {...props}>
        <Datagrid>
            <TextField label="" source="id" />
            <TextField label="Nom" source="name" />
            <TextField label="Prénom" source="username" />
            <EmailField label="Email" source="email" />
        </Datagrid>
    </List>
);